﻿using IRClass.EmployeeOnboarding.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRClass.EmployeeOnboarding.Controllers
{
    public class ApplicationController : Controller
    {              
        public IActionResult Index()
        {
            var applicationModel = new ApplicationViewModel() { 
                Employee = new Models.Employee() , 
                EducationalQualifications = new List<Models.Education> {new Models.Education() },
                WorkExperiences = new List<Models.WorkExperience>(),
                Languages = new List<Models.Language>()
            };
            return View(applicationModel);
        }
    }
}
