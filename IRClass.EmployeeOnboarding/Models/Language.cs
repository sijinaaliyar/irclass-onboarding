﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IRClass.EmployeeOnboarding.Models
{
    public class Language
    {
        [Required]
        [Display(Name = "Language")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Read")]
        public bool Read { get; set; }

        [Required]
        [Display(Name = "Write")]
        public bool Write { get; set; }

        [Required]
        [Display(Name = "Speak")]
        public bool Speak { get; set; }
    }
}
