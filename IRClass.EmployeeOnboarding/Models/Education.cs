﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IRClass.EmployeeOnboarding.Models
{
    public class Education
    {
        [Required(ErrorMessage = "Please enter your qualification")]
        [Display(Name = "Qualification")]
        [StringLength(15)]
        public string QualificationName { get; set; }
       
        [Display(Name = "Specialisation")]
        [StringLength(30)]
        public string Specialisation { get; set; }

        [Display(Name = "Institute")]
        [StringLength(100)]
        public string  Institute { get; set; }

        [Required(ErrorMessage = "Please enter your start date")]
        [DataType(DataType.Date)]
        [Display(Name = "From")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/yyyy}")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Please enter your end date")]
        [DataType(DataType.Date)]
        [Display(Name = "To")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/yyyy}")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Please enter your percentage of marks")]
        [Display(Name = "Percent")]
        [StringLength(10)]
        public float CgpaPercent { get; set; }
    }
}
