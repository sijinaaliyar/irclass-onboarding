﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IRClass.EmployeeOnboarding.Models
{
    public class WorkExperience
    {       
        [Display(Name = "Company")]
        [StringLength(75)]
        public string Company { get; set; }

        [Display(Name = "Designation")]
        [StringLength(75)]
        public string Designation { get; set; }

        [Display(Name = "IndustryType")]
        [StringLength(40)]
        public string IndustryType { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "From")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "To")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime EndDate { get; set; }
    }
}
