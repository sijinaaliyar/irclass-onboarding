﻿using System.ComponentModel.DataAnnotations;

namespace IRClass.EmployeeOnboarding.Models
{
    public class MedicalHistory
    {
        [Display(Name = "Medical History")]
        [DataType(DataType.MultilineText)]
        [StringLength(500)]
        public string MedicalNotes { get; set; }

        [Display(Name = "Physically Challenged")]
        public bool IsPhysicallyChallenged { get; set; }

        [Display(Name = "Nature Of Handicap")]
        [DataType(DataType.MultilineText)]
        [StringLength(200)]
        public string HandicapNature { get; set; }

        [Display(Name = "Had Previous Employment Health Check up?")]
        [StringLength(500)]
        public bool HadPreviousEmploymentHealthCheckups { get; set; }
    }
}
