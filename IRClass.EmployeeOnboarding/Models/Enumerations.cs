﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IRClass.EmployeeOnboarding.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }

    public enum MaritalStatus
    {
        Single,
        Married
    }

    public enum BloodGroup
    {
        [Display(Name = "A+")]
        APos,
        [Display(Name = "B+")]
        BPos,
        [Display(Name = "AB+")]
        ABPos,
        [Display(Name = "O+")]
        OPos,
        [Display(Name = "A-")]
        ANeg,
        [Display(Name = "B-")]
        BNeg,
        [Display(Name = "AB-")]
        ABNeg,
        [Display(Name = "O-")]
        ONeg
    }

    public enum Company
    {
        IRS,
        ISSPL,
        IRQS
    }

    public enum Indicator
    {
        Y,
        N
    }
    public static class EnumExtensions
    {
        public static string ToName(this Enum e)
        {
            var attributes = (DisplayAttribute[])e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false);
            return attributes.Length > 0 ? attributes[0].Name : string.Empty;
        }
    }
}
