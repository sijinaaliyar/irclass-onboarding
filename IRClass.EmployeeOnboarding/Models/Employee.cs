﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IRClass.EmployeeOnboarding.Models
{
    public class Employee
    {
        [Required(ErrorMessage = "Please enter your first name")]
        [Display(Name = "First Name")]
        [StringLength(40)]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Please enter your last name")]
        [Display(Name = "Last Name")]
        [StringLength(40)]
        public string LastName { get; set; }

        [Display(Name = "Maiden Name")]
        [StringLength(50)]
        public string MaidenName { get; set; }

        [Required(ErrorMessage = "Please select your gender")]
        [Display(Name ="Gender")]
        public Gender Gender { get; set; }

        [Display(Name = "Nationality")]
        [StringLength(50)]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Please select your marital status")]
        [Display(Name = "Marital Status")]
        public MaritalStatus MaritalStatus { get; set; }
                
        [Display(Name = "Blood Group")]
        public BloodGroup BloodGroup { get; set; }
        
        [Phone]
        [Required(ErrorMessage = "Please enter your mobile number")]
        [Display(Name = "Mobile No.")]
        public int MobileNumber { get; set; }

        [Phone]
        [Display(Name = "Landline No.")]
        public int LandlineNumber { get; set; }

        [Required]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [RegularExpression(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])",
           ErrorMessage = "The email address is not entered in a correct format")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please select your company")]
        [Display(Name = "Company")] 
        public Company CompanyName { get; set; }

        [Required(ErrorMessage = "Please enter your PAN")]
        [Display(Name = "PAN No.")]
        public string PanNumber { get; set; }

        [Required(ErrorMessage = "Please enter your Aadhar")]
        [Display(Name = "Aadhar No.")]
        public int AadharNumber { get; set; }
                
        [Display(Name = "UAN No")]
        public string UanNumber { get; set; }

        [Required(ErrorMessage = "Please enter your date of birth")]
        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please enter your joining date")]
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Joining")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfJoining { get; set; }

        [Display(Name = "Has Passport")]
        public Indicator HasPassport { get; set; }

        [Display(Name = "Has Driving License")]
        public Indicator HasDrivingLicense { get; set; }
        public Address PresentAddress { get; set; }
        public Address PermanentAddress { get; set; }

    }
   
}
