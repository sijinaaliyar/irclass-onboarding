﻿using IRClass.EmployeeOnboarding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IRClass.EmployeeOnboarding.ViewModels
{
    public class ApplicationViewModel
    {
        public Employee Employee { get; set; }
        public List<Education> EducationalQualifications { get; set; }

        public List<WorkExperience> WorkExperiences { get; set; }

        public MedicalHistory MedicalHistory { get; set; }

        public List<Language> Languages { get; set; }
    }
}
